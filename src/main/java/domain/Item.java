package domain;

public class Item {

    private final String name;
    private final String type;
    private final Float price;


    public String getName() {
        return name;
    }

    public String getType() {
        return type;
    }

    public Float getPrice() {
        return price;
    }


    public Item(String name, String type, Float price) {
        this.name = name;
        this.type = type;
        this.price = price;
    }

}
