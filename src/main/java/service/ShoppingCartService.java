package service;

import domain.Item;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ShoppingCartService {
    private Map<Item, Integer> shoppingCart = new HashMap<>();
    private List<Promotion> promotions = new ArrayList<>();
    private float totalPrice;
    private float totalPriceAfterDiscount;

    public ShoppingCartService() {
    }

    public void addPromotions(Promotion promotion) {
        promotions.add(promotion);
    }

    public List<Promotion> getPromotions() {
        return promotions;
    }

    public void addItem(Item item, Integer quantity) {
        totalPrice += item.getPrice() * quantity;
        totalPriceAfterDiscount += item.getPrice() * quantity;
        if (shoppingCart.containsKey(item)) {
            quantity += shoppingCart.get(item);
        }
        shoppingCart.put(item, quantity);
    }

    public int getQuantityByItem(Item item) {
        return shoppingCart.get(item);
    }

    public int numOfItems() {
        return shoppingCart.size();
    }

    public float getTotalPrice() {
        return totalPrice;
    }

    public float getTotalPriceAfterDiscount() {
        return totalPriceAfterDiscount;
    }

    public void applyPromotion() {
        for (Promotion promotion : promotions) {
            totalPriceAfterDiscount = (totalPriceAfterDiscount - promotion.applyPromotion(shoppingCart));
        }
    }

    public void show() {
        for (Map.Entry<Item, Integer> entry : shoppingCart.entrySet()) {
            System.out.println("Item Name: " + entry.getKey().getName() + " ");
            System.out.println("Type: " + entry.getKey().getType() + " ");
            System.out.println("Quantity: " + entry.getValue() + " ");
            System.out.println("Total Products Price: " + entry.getKey().getPrice() * entry.getValue() + " ");
            System.out.println("------------------------------------");
        }
        System.out.println("Total price: " + totalPrice);
        System.out.println("Total After Discount price: " + totalPriceAfterDiscount);
    }
}
