package service;

import domain.Item;

import java.util.Map;

public class PromotionsOnItems implements Promotion {

    public PromotionsOnItems() {
    }

    @Override
    public float applyPromotion(Map<Item, Integer> shoppingCart) {
        float deduction = 0.0F;
        for (Map.Entry<Item, Integer> entry : shoppingCart.entrySet()) {
            int reminder = 3 % entry.getValue();
            if (entry.getKey().getName().equals("AAA Batteries") && (reminder == 3 || reminder == 0)) {
                int discountItem = 1;
                int n = entry.getValue();
                while (n > 3) {
                    n--;
                    int r = n % 3;
                    if (r == 0) {
                        discountItem++;
                    }
                }
                deduction += discountItem * entry.getKey().getPrice();
            }
        }
        return deduction;
    }
}
