package service;

import domain.Item;

import java.util.Map;

public interface Promotion {
    float applyPromotion(Map<Item, Integer> shoppingCart);
}
