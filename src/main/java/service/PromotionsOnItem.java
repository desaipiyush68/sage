package service;

import domain.Item;

import java.util.Map;

public class PromotionsOnItem implements Promotion {


    public PromotionsOnItem() {
    }

    @Override
    public float applyPromotion(Map<Item, Integer> shoppingCart) {
        float deduction = 0.0F;
        for (Map.Entry<Item, Integer> entry : shoppingCart.entrySet()) {
            float price = entry.getKey().getPrice() * entry.getValue();
            if (entry.getKey().getType().equalsIgnoreCase("Audio")) {
                deduction += (price - (price * 30 / 100));
            }
        }

        return deduction;
    }
}
