package service;


import domain.Item;
import org.junit.Assert;
import org.junit.Test;

public class ShoppingCartServiceTest {


    @Test
    public void addItem_should_add_item_in_shoppingCart() {
        ShoppingCartService service = new ShoppingCartService();
        Item item = new Item("Headphones", "Audio", 150.00F);
        service.addItem(item, 1);
        Assert.assertEquals(1, service.numOfItems());
    }


    @Test
    public void addItem_should_increase_quantity() {
        ShoppingCartService service = new ShoppingCartService();
        Item item = new Item("Headphones", "Audio", 150.00F);
        service.addItem(item, 1);
        service.addItem(item, 1);
        Assert.assertEquals(2, service.getQuantityByItem(item));
        Assert.assertEquals(300.0F, service.getTotalPrice(), 0.0);
    }

    @Test
    public void addItem_should_price_to_totalPrice() {
        ShoppingCartService service = new ShoppingCartService();
        Item item = new Item("Headphones", "Audio", 150.00F);
        service.addItem(item, 2);
        Assert.assertEquals(300.0F, service.getTotalPrice(), 0.0);
    }

    @Test
    public void addPromotion_should_add_promotion_in_promotions() {
        PromotionsOnItem promotionsOnItem = new PromotionsOnItem();
        ShoppingCartService service = new ShoppingCartService();
        service.addPromotions(promotionsOnItem);
        Assert.assertEquals(promotionsOnItem, service.getPromotions().get(0));
    }


    @Test
    public void applyPromotion_should_apply_promotion_on_shoppingCart() {
        PromotionsOnItem promotionsOnItem = new PromotionsOnItem();
        PromotionsOnItems promotionsOnItems = new PromotionsOnItems();
        ShoppingCartService service = new ShoppingCartService();
        Item item = new Item("Headphones", "Audio", 150.0F);
        Item item2 = new Item("Speakers", "Audio", 85.0F);
        Item item3 = new Item("AAA Batteries", "Power", 0.85F);
        service.addPromotions(promotionsOnItem);
        service.addPromotions(promotionsOnItems);
        service.addItem(item, 2);
        service.addItem(item2, 1);
        service.addItem(item3, 3);
        Assert.assertEquals(service.getTotalPrice(), service.getTotalPriceAfterDiscount(), 0.0);
        service.applyPromotion();
        Assert.assertEquals(117.19998931884766, service.getTotalPriceAfterDiscount(), 0.0);
        service.show();
    }

}